class RPNCalculator
  def initialize
     @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    raise_error
    perform_operation(:+)
  end

  def minus
    raise_error
    perform_operation(:-)
  end

  def times
    raise_error
    perform_operation(:*)
  end

  def divide
    raise_error
    perform_operation(:/)
  end

  def value
    @stack.last
  end

  def tokens(str)
    result = str.split.map { |el| is_operation?(el) ? el.to_sym : el.to_i }
    result
  end

  def evaluate(str)
    tokens = tokens(str)
    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    value

  end

  private

  def raise_error
    if @stack.size < 2
      raise "calculator is empty"
    end
  end

  def is_operation?(el)
    operations = "+-/*"
    if operations.include?(el)
      true
    else
      false
    end
  end

  def perform_operation(operation)
    raise_error
    second_operand = @stack.pop
    first_operand = @stack.pop

    case operation
    when :+
      @stack << first_operand + second_operand
    when :-
      @stack << first_operand - second_operand
    when :*
      @stack << first_operand * second_operand
    when :/
      @stack << first_operand.fdiv(second_operand)
    else
      @stack << first_operand
      @stack << second_operand
      raise "No such operation as #{operation}"
    end
  end

end
